import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

// export interface Books {
//   title: string;
//   author: string[];
// }

@Injectable()
export class GetBooks {

  readonly SERVER = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {}

  // Get all books fn to be used on search btn event.
  getALLbooks(config = {}): Promise<any> {
    // Setting the query string
    // GET /books?title=<text>?author=<text>
    const qs = new HttpParams()
      .set('title', config['title'])
      .set('lastname', config['lastname'])
      .set('firstname', config['firstname']);

    return (
      this.httpClient.get(`${this.SERVER}/books`, { params: qs})
        .take(1).toPromise()
    );
  }
}
