import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { GetBooks } from '../book.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books = [{
    title: "",
    lastname: "",
    firstname: ""
    }];

  constructor(private getBooks: GetBooks, private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  private loadBook() {
    this.getBooks.getALLbooks({})
      .then((result) => {this.books = result})
      .catch((error) => {console.error(error); });
  }

  showDetails(title: string) {
    console.log('> title: %s', title);
    this.router.navigate(['/book', title]);
  }

}
