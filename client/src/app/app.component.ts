import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GetBooks } from './book.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild(NgForm) bookForm: NgForm;

  constructor(private getBooks: GetBooks) {}

  books = [];

  ngOnInit() {}

  _getBooks(bookForm: NgForm) {
    const title = this.bookForm.value['title'];
    const lastname = this.bookForm.value['lastname'];
    const firstname = this.bookForm.value['firstname'];

    // Pass on to the service to process.
    this.getBooks.getALLbooks({ title, lastname, firstname})
      .then( result => {
        console.log('>>> result: ', result);
        this.books = result
        })
      .catch(error => {
        console.log('error: ', error);
        })
  }


}
