// Load the libraries.
const path = require('path');
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

// Create a pool connection to MySQL database.
const pool = mysql.createPool({
    host: 'localhost', port: 3306,
    user: 'fred', password: 'Internet1',
    database: 'library_books',
    connectionLimit: 4
});

const bkQuery = (SQL, pool) => {
    return (params) => {
        const p = new Promise((resolve, reject) => {
            console.log("In closure: ", SQL);
            pool.getConnection((err, conn) => {
                if (err) {
                    reject({status: 500, error: err}); return;
                }
                conn.query(SQL, params || [],
                    (err, result) => {
                        try {
                            if (err)
                                reject(err);
                            else
                                reject({status: 400, error: err}); return;
                        } finally {
                            conn.release();
                        }
                    }
                )
            })
        })
        return (p);
    }
}

// Instantiate express.
const app = express();
// Load CORS into app.
app.use(cors());

// Route handlers; search for Title & Author.
const SELECT_BOOKS = 'select * from library_books.books where title = ? limit 10';

// Instantiate bkQuery with the query string and pool configuration.
const selectBooks = bkQuery(SELECT_BOOKS, pool);

// GET books/?title=<text>?lastname=<text>?firstname=<text>
app.get('/books', (req, resp) => {

    const title = req.query.title;
    const lastname = req.query.lastname;
    const firstname = req.query.firstname;

    const limit = req.query.limit|| 10;
    const offset = req.query.offset|| 0;

    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_BOOKS, [ req.query.title ],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results).end(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
});

// Start the application.
const PORT = 3000;
app.listen(PORT, () => {
    console.log('Application started on port %d', PORT);
});
